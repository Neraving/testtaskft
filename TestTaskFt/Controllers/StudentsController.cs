﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestTaskFt.Commands;
using TestTaskFt.Context;
using TestTaskFt.Models;
using TestTaskFt.Query;
using TestTaskFt.RequestResult;

namespace TestTaskFt.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        #region Initialization
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public StudentsController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }
        #endregion

        #region Get
        /// <summary>
        /// Get all students
        /// </summary>
        [HttpGet, Route("GetAll")]
        public async Task<IEnumerable<StudentDto>> Get()
        {
            try
            {
                var result = await _mediator.Send(new AllStudentsQuery());
                return _mapper.Map<StudentDto[]>(result);
            }
            catch
            {

            }
            return new StudentDto[] { };
        }

        /// <summary>
        /// Get student by id.
        /// </summary>
        [HttpGet, Route("GetStudentByIdQuery")]
        public async Task<StudentDto> GetStudentById([FromQuery]GetStudentByIdQuery query)
        {
            try
            {
                var result = await _mediator.Send(query);
                return _mapper.Map<StudentDto>(result);
            }
            catch
            {

            }
            return new StudentDto();
        }
        #endregion

        #region Post
        /// <summary>
        /// Add new student.
        /// </summary>
        [HttpPost(Name = "AddNewStudent")]
        public async Task Add([FromBody]AddStudentCommand command)
        {
            try
            {
                if (command.FirstName != null)
                {
                    await _mediator.Send(command);
                    return;
                }
                throw new ArgumentNullException("No data inside");
            }
            catch
            {

            }
        }
        #endregion
    }
}

