﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTaskFt.Context;

namespace TestTaskFt
{
    public static class SeedExtension
    {
        public static IHost Seed(this IHost host)
        {
            using (var scope = host.Services.GetService<IServiceScopeFactory>().CreateScope())
            {
                using (var dbContext = scope.ServiceProvider.GetRequiredService<StudentDbContext>())
                {
                    SeedData.SeedAsync(dbContext).GetAwaiter().GetResult();

                    return host;
                }
            }
            return host;
        }

        public static class SeedData
        {


            public static async Task SeedAsync(StudentDbContext context)
            {
                if (context.Students.Any())
                {
                    return;  
                }
                var dbSeed = new StudentDbInitializer();
                var students = dbSeed.GenerateSeed().ToList();
                context.AddRange(students);
                context.SaveChanges();
            }
        }
    }
}
