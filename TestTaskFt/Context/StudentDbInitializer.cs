﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTaskFt.Models;

namespace TestTaskFt.Context
{
    public class StudentDbInitializer
    {
        private static readonly string[] FirstNames = new[]
{
            "Addison", "Emma", "Gabriel", "Louise", "Jade", "Raphael", "Alice", "Leo", "Chloe", "Lucas"
        };

        private static readonly string[] LastNames = new[]
{
            "Smith", "Johnson", "Williams", "Brown", "Jones", "Garcia", "Miller", "Davis", "Rodriguez", "Martinez"
        };

        private static readonly string[] MiddleNames = new[]
{
            "Addisons", "Emmas", "Gabriels", "Louises", "Jades", "Raphaels", "Alices", "Leos", "Chloes", "Lucases"
        };

        public IEnumerable<Student> GenerateSeed()
        {
            var students = new List<Student>();
            var booksNumbers = new List<int>();
            var rnd = new Random();

            for (var i = 0; i < 10; i++)
            {
                var fname = FirstNames[rnd.Next(FirstNames.Length)];
                var lname = LastNames[rnd.Next(LastNames.Length)];
                var mname = MiddleNames[rnd.Next(MiddleNames.Length)];
                var email = String.Format("{0}.{1}.{2}@gmail.com", fname[0], mname[0], lname);
                var bookNumber = GenerateNextBookNumber(rnd, booksNumbers);
                students.Add(new Student
                {
                    FirstName = fname,
                    LastName = lname,
                    MiddleName = mname,
                    Birthday = DateTime.Now.AddDays(-rnd.Next(1, 365)),
                    Email = email,
                    RecordBookNumber = bookNumber,
                });
            }
            return students;
        }

        private int GenerateNextBookNumber(Random rnd, List<int> currentSet)
        {
            var possibleNumber = rnd.Next(100000, 1000000);
            return !currentSet.Contains(possibleNumber) ? possibleNumber : GenerateNextBookNumber(rnd, currentSet);
        }
    }
}
