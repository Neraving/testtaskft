﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTaskFt.Context;
using TestTaskFt.Models;
using MediatR;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using AutoMapper.QueryableExtensions;

namespace TestTaskFt.Query
{
    public class GetStudentByIdHandler : IRequestHandler<GetStudentByIdQuery, Student>
    {
        private readonly StudentDbContext _context;

        public GetStudentByIdHandler(StudentDbContext context)
        {
            _context = context;
        }

        public async Task<Student> Handle(GetStudentByIdQuery request, CancellationToken cancellationToken)
        {
            return await _context.Students.FirstOrDefaultAsync(x => x.Id == request.Id);
        }
    }
}
