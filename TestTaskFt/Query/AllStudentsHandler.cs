﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTaskFt.Context;
using TestTaskFt.Models;
using MediatR;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using AutoMapper.QueryableExtensions;

namespace TestTaskFt.Query
{
    public class AllStudentsHandler : IRequestHandler<AllStudentsQuery, Student[]>
    {
        private readonly StudentDbContext _context;

        public AllStudentsHandler(StudentDbContext context)
        {
            _context = context;
        }

        public async Task<Student[]> Handle(AllStudentsQuery request, CancellationToken cancellationToken)
        {
            return await _context.Students.ToArrayAsync();
        }
    }
}
