﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTaskFt.Models;
using MediatR;

namespace TestTaskFt.Query
{
    public class GetStudentByIdQuery : IRequest<Student>
    {
        public Guid Id { get; set; }
    }
}
