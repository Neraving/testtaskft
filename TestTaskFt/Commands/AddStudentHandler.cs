﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using TestTaskFt.Context;
using TestTaskFt.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace TestTaskFt.Commands
{
    public class AddStudentHandler : IRequestHandler<AddStudentCommand>
    {
        private readonly StudentDbContext _context;

        public AddStudentHandler(StudentDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(AddStudentCommand request, CancellationToken cancellationToken)
        {
            //TODO СОДОМИЯ, но все варианты фигня
            var isAlreadyTaken = _context.Students
                .Any(u => u.RecordBookNumber == request.RecordBookNumber);

            if (isAlreadyTaken)
            {
                throw new ArgumentException("Record book number already taken.");
            }
            var student = new Student()
            {
                FirstName = request.FirstName,
                RecordBookNumber = request.RecordBookNumber
            };
            _context.Students.Add(student);
            _context.SaveChangesAsync();
            return new Unit();
        }


    }
}
