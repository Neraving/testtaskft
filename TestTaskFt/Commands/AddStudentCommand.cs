﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;

namespace TestTaskFt.Commands
{
    public class AddStudentCommand : IRequest
    {
        public string FirstName { get; set; }
        public int RecordBookNumber { get; set; }
    }
}
