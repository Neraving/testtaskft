﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTaskFt.Models;
using TestTaskFt.RequestResult;

namespace TestTaskFt.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Student, StudentDto>()
                .ForMember("FullName",
                opt => opt.MapFrom(x =>
                    (x.FirstName + " " + x.MiddleName + " " + x.LastName).Trim()));
        }
    }
}
